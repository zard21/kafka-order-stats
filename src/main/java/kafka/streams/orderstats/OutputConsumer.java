package kafka.streams.orderstats;

import kafka.streams.orderstats.model.OrderStats;
import kafka.streams.orderstats.serde.OrderStatsDeserializer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.Arrays;
import java.util.Properties;

public class OutputConsumer {

    public static void main(String[] args) {
        Properties properties = new Properties();

        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, OrderStatsDeserializer.class.getName());

        properties.setProperty("group.id", "test");
        properties.setProperty("enable.auto.commit", "false");
        properties.setProperty("auto.offset.reset", "earliest");

        KafkaConsumer<String, OrderStats> kafkaConsumer = new KafkaConsumer<>(properties);
        kafkaConsumer.subscribe(Arrays.asList("test-order-output"));

        while (true) {
            ConsumerRecords<String, OrderStats> consumerRecords = kafkaConsumer.poll(100);
            for (ConsumerRecord<String, OrderStats> consumerRecord : consumerRecords) {
                System.out.println(
//                        "Partition: "   + consumerRecord.partition() +
//                        ", Topic: "     + consumerRecord.topic() +
//                        ", Offset: "    + consumerRecord.offset() +
                        ", Key: "       + consumerRecord.key() +
                        ", Value: "     + consumerRecord.value() +
                        ", Timestamp: " + consumerRecord.timestamp()
                );
            }
            kafkaConsumer.commitSync();
        }
    }
}
