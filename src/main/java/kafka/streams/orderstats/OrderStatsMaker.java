package kafka.streams.orderstats;

import kafka.streams.orderstats.model.Order;
import kafka.streams.orderstats.model.OrderStats;
import kafka.streams.orderstats.serde.*;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.TimeWindows;

import java.util.Properties;
import java.util.concurrent.ThreadLocalRandom;

public class OrderStatsMaker {

    public static void main(String[] args) {

        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "kafka-streams-order-stats");
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, OrderSerde.class);
        config.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 0);
        config.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);

        StreamsBuilder builder = new StreamsBuilder();

        KStream<String, Order> orderStream = builder.stream("test-order-input");

        KStream<String, OrderStats> statsStream = orderStream
                .groupByKey()
                .windowedBy(
                        TimeWindows
                                .of(4000)
                                .advanceBy(1000)
                )
                .aggregate(
                        OrderStats::new,
                        ((key, value, orderStats) -> orderStats.add(value)),
                        Materialized.with(Serdes.String(), new OrderStatsSerde())
                )
                .toStream((key, value) -> (key.key() + "-" + Long.toString(key.window().start() / 1000)))
                .mapValues(OrderStats::calcAvg);
        statsStream.to("test-order-output", Produced.with(Serdes.String(), new OrderStatsSerde()));

        KafkaStreams streams = new KafkaStreams(builder.build(), config);
        streams.start();

        // Producer
        Properties producerConfig = new Properties();

        producerConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        producerConfig.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        producerConfig.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, OrderSerializer.class.getName());

        Producer<String, Order> producer = new KafkaProducer<>(producerConfig);

        while (true) {
            try {
                Order order = new Order(System.currentTimeMillis() / 1000,
                        ThreadLocalRandom.current().nextInt(0, 100),
                        ThreadLocalRandom.current().nextInt(0, 100000));
                ProducerRecord<String, Order> record = new ProducerRecord<>("test-order-input", "BTC_KRW", order);
                producer.send(record);

                Thread.sleep(500L);
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
        }

        producer.close();
    }
}
