package kafka.streams.orderstats.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderStats {

    @JsonProperty("totalCount")
    int totalCount;
    @JsonProperty("sumPrice")
    double sumPrice;
    @JsonProperty("minPrice")
    double minPrice;
    @JsonProperty("maxPrice")
    double maxPrice;
    @JsonProperty("avgPrice")
    double avgPrice;
    @JsonProperty("openPrice")
    double openPrice;
    @JsonProperty("closePrice")
    double closePrice;
    @JsonIgnore
    long openTime;
    @JsonIgnore
    long closeTime;

    public OrderStats() {
        this.totalCount = 0;
        this.sumPrice = 0;
        this.minPrice = 0;
        this.maxPrice = 0;
        this.avgPrice = 0;
        this.openPrice = 0;
        this.closePrice = 0;
        this.openTime = 0L;
        this.closeTime = 0L;
    }

    public OrderStats add(Order order) {
        if (totalCount == 0) {
            this.minPrice = order.getPrice();
            this.maxPrice = order.getPrice();
            this.openPrice = order.getPrice();
            this.closePrice = order.getPrice();
            this.openTime = order.getTimestamp();
            this.closeTime = order.getTimestamp();
        }

        this.totalCount = this.totalCount + order.getCount();
        this.sumPrice = this.sumPrice + (order.getPrice() * order.getCount());
        this.minPrice = this.minPrice < order.getPrice() ? this.minPrice : order.getPrice();
        this.maxPrice = this.maxPrice < order.getPrice() ? order.getPrice() : this.maxPrice;

        this.openPrice = this.openTime < order.getTimestamp() ? this.openPrice : order.getPrice();
        this.closePrice = this.closeTime < order.getTimestamp() ? order.getPrice() : this.closePrice;

        return this;
    }

    public OrderStats calcAvg() {
        this.avgPrice = this.sumPrice / this.totalCount;

        return this;
    }

    @Override
    public String toString() {
        return "OrderStats{" +
                ", totalCount=" + totalCount +
                ", sumPrice=" + sumPrice +
                ", minPrice=" + minPrice +
                ", maxPrice=" + maxPrice +
                ", avgPrice=" + avgPrice +
                ", openPrice=" + openPrice +
                ", closePrice=" + closePrice +
                ", openTime=" + openTime +
                ", closeTime=" + closeTime +
                '}';
    }
}
