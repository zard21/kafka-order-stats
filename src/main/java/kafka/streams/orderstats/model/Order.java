package kafka.streams.orderstats.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Order {

    @JsonProperty("timestamp")
    long timestamp;
    @JsonProperty("price")
    double price;
    @JsonProperty("count")
    int count;

    public Order() {
        this.timestamp = 0L;
        this.price = 0;
        this.count = 0;
    }

    public Order(long timestamp, double price, int count) {
        this.timestamp = timestamp;
        this.price = price;
        this.count = count;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public double getPrice() {
        return price;
    }

    public int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "Order{" +
                "timestamp=" + timestamp +
                ", price=" + price +
                ", count=" + count +
                '}';
    }
}
