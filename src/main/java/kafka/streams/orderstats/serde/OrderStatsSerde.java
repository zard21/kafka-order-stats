package kafka.streams.orderstats.serde;

import kafka.streams.orderstats.model.Order;
import kafka.streams.orderstats.model.OrderStats;
import org.apache.kafka.common.serialization.Serde;

import java.util.Map;

public class OrderStatsSerde implements Serde<OrderStats> {

    final private OrderStatsSerializer serializer;
    final private OrderStatsDeserializer deserializer;

    public OrderStatsSerde() {
        this.serializer = new OrderStatsSerializer();
        this.deserializer = new OrderStatsDeserializer();
    }

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        serializer.configure(configs, isKey);
        deserializer.configure(configs, isKey);
    }

    @Override
    public void close() {
        serializer.close();
        deserializer.close();
    }

    @Override
    public OrderStatsSerializer serializer() {
        return serializer;
    }

    @Override
    public OrderStatsDeserializer deserializer() {
        return deserializer;
    }
}
