package kafka.streams.orderstats.serde;

import kafka.streams.orderstats.model.Order;
import org.apache.kafka.common.serialization.Serde;

import java.util.Map;

public class OrderSerde implements Serde<Order> {

    final private OrderSerializer serializer;
    final private OrderDeserializer deserializer;

    public OrderSerde() {
        this.serializer = new OrderSerializer();
        this.deserializer = new OrderDeserializer();
    }

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        serializer.configure(configs, isKey);
        deserializer.configure(configs, isKey);
    }

    @Override
    public void close() {
        serializer.close();
        deserializer.close();
    }

    @Override
    public OrderSerializer serializer() {
        return serializer;
    }

    @Override
    public OrderDeserializer deserializer() {
        return deserializer;
    }
}
