package kafka.streams.orderstats.serde;

import com.fasterxml.jackson.databind.ObjectMapper;
import kafka.streams.orderstats.model.Order;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

public class OrderDeserializer implements Deserializer<Order> {

    @Override
    public Order deserialize(String arg0, byte[] orderBytes) {
        ObjectMapper mapper = new ObjectMapper();
        Order order = null;
        try {
            order = mapper.readValue(orderBytes, Order.class);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return order;
    }

    @Override
    public void close() {
        // TODO Auto-generated method stub
    }

    @Override
    public void configure(Map<String, ?> arg0, boolean arg1) {
        // TODO Auto-generated method stub
    }
}
