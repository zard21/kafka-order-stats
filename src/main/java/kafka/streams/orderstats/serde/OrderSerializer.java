package kafka.streams.orderstats.serde;

import com.fasterxml.jackson.databind.ObjectMapper;
import kafka.streams.orderstats.model.Order;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class OrderSerializer implements Serializer<Order> {

    @Override
    public byte[] serialize(String arg0, Order order) {
        byte[] serializedBytes = null;

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            serializedBytes = objectMapper.writeValueAsString(order).getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return serializedBytes;
    }

    @Override
    public void close() {
        // TODO Auto-generated method stub
    }

    @Override
    public void configure(Map<String, ?> arg0, boolean arg1) {
        // TODO Auto-generated method stub
    }
}
