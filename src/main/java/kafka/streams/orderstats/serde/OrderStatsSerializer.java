package kafka.streams.orderstats.serde;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import kafka.streams.orderstats.model.Order;
import kafka.streams.orderstats.model.OrderStats;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class OrderStatsSerializer implements Serializer<OrderStats> {

    @Override
    public byte[] serialize(String arg0, OrderStats order) {
        byte[] serializedBytes = null;

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        try {
            serializedBytes = objectMapper.writeValueAsString(order).getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return serializedBytes;
    }

    @Override
    public void close() {
        // TODO Auto-generated method stub
    }

    @Override
    public void configure(Map<String, ?> arg0, boolean arg1) {
        // TODO Auto-generated method stub
    }
}
