package kafka.streams.orderstats.serde;

import com.fasterxml.jackson.databind.ObjectMapper;
import kafka.streams.orderstats.model.Order;
import kafka.streams.orderstats.model.OrderStats;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

public class OrderStatsDeserializer implements Deserializer<OrderStats> {

    @Override
    public OrderStats deserialize(String arg0, byte[] orderBytes) {
        ObjectMapper mapper = new ObjectMapper();
        OrderStats stats = null;
        try {
            stats = mapper.readValue(orderBytes, OrderStats.class);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return stats;
    }

    @Override
    public void close() {
        // TODO Auto-generated method stub
    }

    @Override
    public void configure(Map<String, ?> arg0, boolean arg1) {
        // TODO Auto-generated method stub
    }
}
